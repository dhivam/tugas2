<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyController extends Controller
{
    public function index(){
        return view ('Hello');
    }

    public function data()
    {
        $activity = [
            ['time'=> 'Morning', 'do'=> 'Excersice, Breakfast & Online Class'],
            ['time'=> 'Afternoon', 'do'=> 'Online Class, Break & Lunch '],
            ['time'=> 'Night', 'do'=> 'Do Homework, Dinner & Sleep']
        ];
        return view ('activity', compact('activity'));
    }
}
