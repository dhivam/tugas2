@extends('layout.main')

@section('title', 'activity')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
            <h1 class="mt-3">My Daily Activity </h1>
                @foreach ($activity as $act)
                    <p><strong>{{ $act['time'] }}</strong></p>
                    <p>{{ $act['do'] }}</p>
                @endforeach
            </div>
        </div>
    </div>
@endsection
 